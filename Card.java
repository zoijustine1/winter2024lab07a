public class Card {
	private String suit;
	private String value;
	private double score;
	
	//Create a constructor
	public Card(String value, String suit,double score){
		this.suit = suit;
		this.value = value;
		this.score = score;
	}
	
	//Create get methods
	public String getSuit(){
		return this.suit;
	}
	public String getValue(){
		return this.value;
	}
	
	//create toString method
	public String toString(){
		return this.value + " of " + this.suit + "\n" + this.score;
	}
	
	public double calculateScore(){
		if(this.suit.equals("Hearts")){
			this.score += 0.4;
		}
		else if(this.suit.equals("Spades")){
			this.score += 0.3;
		}
		else if(this.suit.equals("Diamonds")){
			this.score += 0.2;
		}
		else {
			this.score +=0.1;
		}
		return this.score;
	}
}
		