public class SimpleWar {
	public static void main(String[] args){
		Deck deckOfCards = new Deck();
		deckOfCards.shuffle();
		System.out.println(deckOfCards);
		int playerOnePoints = 0;
		int playerTwoPoints = 0;
		
		//This method compares the cards of the two players and if player 1 has more than player 2, the points of player 1 goes up and vice versa. This loop will continue until
		// there's not cards left in the deck.
		int i = 1;
		while(deckOfCards.length() != 0){
			System.out.println("Round " + i);
			Card cardNumOne = deckOfCards.drawTopCard();
			System.out.println("Player 1 card: " + cardNumOne);
			System.out.println(cardNumOne.calculateScore());
			
			Card cardNumTwo = deckOfCards.drawTopCard();
			System.out.println("Player 2 card: " + cardNumTwo);
			System.out.println(cardNumTwo.calculateScore());
			
			if(cardNumOne.calculateScore() > cardNumTwo.calculateScore()){
				playerOnePoints++;
				System.out.println("Player 1 wins with " + playerOnePoints + " points");
			}
			else {
				playerTwoPoints++;
				System.out.println("Player 2 wins with " + playerTwoPoints + " points");
			}
			i++;
		}
		if(playerOnePoints > playerTwoPoints){
			System.out.println("Congrats! Player 1 wins with total " + playerOnePoints + " points");
		}
		else if(playerOnePoints < playerTwoPoints){
			System.out.println("Congrats! Player 2 wins with total" + playerTwoPoints + " points");
		}
		else {
			System.out.println("Both players have the same points: " + playerOnePoints + " points");
		}
	}
}