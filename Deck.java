import java.util.Random;
public class Deck {
	private Card[] cards;
	private int numberOfCards;
	private Random rng;
	
	
	//create a constructor
	public Deck(){
		this.rng = new Random();
		this.numberOfCards = 52;
		this.cards = new Card[this.numberOfCards];
		String[] values = {"Ace","Two","Three","Four","Five","Six","Seven","Eight","Nine","Ten","Jack","Queen","King"};
		String[] suits = {"Hearts","Spades","Diamonds","Clubs"};
		//String suits = "Hearts";
		int cardPosition = 0;
		for(int i = 0; i <values.length;i++){
			for(int j = 0;j<suits.length;j++){
				cards[cardPosition] = new Card(values[i],suits[j],(i+1.0));
				//System.out.println(cards[cardPosition]);
				cardPosition++;
			}
		}
	}
	
	//create a length method and returning the numberOfCards because 
	// we want to be able to remove the some of the cards to know how many cards are left.
	public int length() {
		return this.numberOfCards;
	}
	
	//create drawTopCard method to draw the last card in the array
	public Card drawTopCard(){
		this.numberOfCards--;
		return this.cards[this.numberOfCards];
	}

	//create a toString method to print the cards left. 
	public String toString(){
		String builder = "";
		for(int i = 0;i<this.numberOfCards;i++){
			builder += cards[i]+"\n";
		}
		return builder;
	}
	//create a shuffle method to shuffle the cards in random position
	public void shuffle(){
		for(int i = 0;i <this.numberOfCards;i++){
			int randNum = this.rng.nextInt(this.numberOfCards -i);
			Card swap = this.cards[i];
			this.cards[i] = this.cards[randNum];
			this.cards[randNum] = swap;
		}
	}
}
	